#!/box/bin/php
<?php
/**
 * Copyright (c) 2013, Jordan Sterling.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
 * associated documentation files (the "Software"), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial
 * portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 * WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


/**
 * Tracks growing directories.
 *
 * Usage:
 *
 * This is best to put on a cron so it is run periodically. One run of the script will not
 * produce anything useful. You need to run the script twice to see if any directories have
 * grown. run with no arguments for usage.
 *
 * growing.php >> /var/log/growlogs.txt
 *
 * Your logs should be rotated on each run
 *
 * This script requires a storage location on disk to store the old directory sizes.
 * By default it uses /tmp, however you can provide an alternative.
 */
class growing {

        const Default_Previous_Run_Storage_File = '/tmp/growing_previous_run_storage_file';
        const Version_Number = '1.0';

        /**
         * Ignores directories from inclusion in the grow monitors.
         * @var array
         */
        private $ignore_list = array();

        /**
         * The directory you are watching for growing folders.
         * @var String
         */
        private $directory_to_watch;

        /**
         * Where to store the previous runs directory sizes. Every time
         * the script runs it needs to store the state of all directories
         * that are scanned.
         * @var String
         */
        private $previous_run_storage_file;

        /**
         * Parses command line args
         * @param array arguments The command line arguments
         */
        public function parse_command_line_arguments(array $arguments) {
                // no args: show help
                if (count($arguments) === 1) {
                        $this->help();
                }
                $options = getopt("i:d:t:?", array(
                        'ignore:',
                        'directory:',
                        'tmp-storage:',
                        'help',
                ));

                // error processing options.. weird..
                if (!$options) {
                        $this->help("Error occurred processing arguments");
                }

                // -d, --directory
                if ($options['d']) {
                        $this->directory_to_watch = $options['d'];
                }
                else if ($options['directory']) {
                        $this->directory_to_watch = $options['directory'];
                }
                else {
                        $this->help("ERROR: Missing directory argument. -d or --directory is required");
                }

                // -t, --tmp-location
                if ($options['t']) {
                        $this->previous_run_storage_file = $options['t'];
                }
                else if ($options['tmp-location']) {
                        $this->previous_run_storage_file = $options['tmp-location'];
                }
                else {
                        $this->previous_run_storage_file = self::Default_Previous_Run_Storage_File;
                }

                $ignore_options = array('i', 'ignore');
                foreach ($ignore_options as $key) {
                        if (!is_array($options[$key])) {
                                $this->ignore_list[] = $options[$key];
                        }
                        else {
                                foreach ($options[$key] as $ignored_file) {
                                        $this->ignore_list[] = $ignored_file;
                                }
                        }
                }

                // -?, --help
                if ($options['?'] || $options['help']) {
                        $this->help();
                }
        }

        /**
         * Grow checking. This uses =& for assignment because it is dealing with large arrays,
         * there is no need to make a copy of these after each function call.
         */
        public function run() {
                $current_directory_sizes =& $this->get_current_directory_sizes();
                $growing_directories =& $this->compare_current_sizes_to_last_run($current_directory_sizes);
                $this->write_to_previous_run_storage_file($current_directory_sizes);
                $this->print_growing_directories($growing_directories);
        }


        /**
         * Takes an output line of the du program and turns it into
         * an array that has a byte count and the directory name.
         */
        private function split_du_output_line($line) {
                // the du output looks like
                // [number][whitespace][directory name]
                // we want to extract the number and directory name.
                //
                // split on whitespace, take the first chunk as the bytes and the rest of the
                // chunks as the name.
                $du_regex = '/^(?P<bytes>[0-9]+)[\s]+(?P<filename>.*)$/';
                $matches = array();
                if (!preg_match($du_regex, $line, $matches)) {
                        return array();
                }
                return array(
                        'bytes' => $matches['bytes'],
                        'filename' => $matches['filename'],
                );
        }

        /**
         * Determines if a directory is in the ignore list
         *
         * @param String directory
         * @return Boolean
         */
        private function is_directory_ignored($directory) {
                foreach ($this->ignore_list as $ignored_directory) {
                        if (strpos($directory, $ignored_directory) === 0) {
                                return true;
                        }
                }
                return false;
        }

        /**
         * Does a du on the directory to watch to get the current folder sizes. This
         * filters the results by ignoring files and anything in the ignore list.
         *
         * This gives back an associative array whose keys are the filenames and the values
         * are the size in bytes.
         *
         * @return array
         */
        private function get_current_directory_sizes() {
                $cmd = 'du -b ';
                $cmd .= escapeshellarg($this->directory_to_watch);
                $du_results = array();
                $exit_code = 0;
                exec($cmd, $du_results, $exit_code);
                if ($exit_code !== 0) {
                        error_log(sprintf("ERROR: Unable to execute du to get current directory sizes. Tried command: %s\nSaw exit code: %d\n", $cmd, $exit_code));
                        exit(7);
                }

                // -- if directory, save it in an array where the key is the directory, the value is the size
                $current_directory_sizes = array();
                foreach ($du_results as $du_output_line) {
                        $result = $this->split_du_output_line($du_output_line);
                        $filename = $result['filename'];
                        $bytes = $result['bytes'];

                        // skip non-dirs and ignored dirs
                        if (!is_dir($filename) || $this->is_directory_ignored($filename)) {
                                continue;
                        }
                        $current_directory_sizes[$filename] = $bytes;
                }
                return $current_directory_sizes;
        }

        /**
         * Reads the file which has the last run's directory sizes and compares them to the
         * current sizes. If any directories are bigger now than they were previously, they will
         * be returned in an array. The keys are the file names and the values are the size
         * in bytes of how much they grew by.
         *
         * If the last run file does not exist, then no directories will be returned.
         *
         * @param array current_directory_sizes This is pass by reference just to not make a copy
         *                                      of it, it is not modified in this function.
         * @return array The list of directories that have grown since the previous run
         */
        private function compare_current_sizes_to_last_run(array &$current_directory_sizes) {
                $growers = array();
                if (is_readable($this->previous_run_storage_file) && filesize($this->previous_run_storage_file) > 0) {
                        $fh = fopen($this->previous_run_storage_file, 'r');
                        if (!$fh) {
                                error_log("ERROR: Unable to open previous run storage file for reading: {$this->previous_run_storage_file}\n");
                                exit(2);
                        }
                        while (!feof($fh))
                        {
                                $line = fgets($fh);
                                if (feof($fh)) {
                                        break;
                                }
                                if (!$line) {
                                        error_log("ERROR: Unable to read from previous run storage file: {$this->previous_run_storage_file}\n");
                                        exit(3);
                                }
                                $result = $this->split_du_output_line($line);
                                $size_last_run = $result['bytes'];
                                $filename = $result['filename'];

                                if (!isset($current_directory_sizes[$filename])) {
                                        // a directory existed previously but does not exist now... abort
                                        continue;
                                }
                                $size_today = $current_directory_sizes[$filename];
                                if ($size_today > $size_last_run) {
                                        // grower found. store the name and how much it grew by
                                        $growers[$filename] = $size_today - $size_last_run;
                                }
                        }
                        if (!fclose($fh)) {
                                error_log("ERROR: Unable to close previous storage file: {$this->previous_run_storage_file}\n");
                                exit(3);
                        }
                }
                ksort($growers);
                return $growers;
        }

        /**
         * Writes the current directory sizes to the 'previous run storage file' location.
         * This lets the next invocation of the script know how big the directories were now
         * so it can do the comparison.
         *
         * The format of each line of the file looks like:
         * [size in bytes]\t[filename]\n
         *
         * @param array current_directory_sizes The sizes of the directories now. This is pass by reference just to
         *                                      not make a copy of it, it is not modified in this function.
         */
        private function write_to_previous_run_storage_file(array &$current_directory_sizes) {
                $fh = fopen($this->previous_run_storage_file, 'w');
                if (!$fh) {
                        error_log("ERROR: Unable to open previous storage file for writing: {$this->previous_run_storage_file}\n");
                        exit(4);
                }
                foreach ($current_directory_sizes as $filename => $bytes) {
                        $string_to_write = "$bytes $filename" . PHP_EOL;
                        $bytes_to_write = strlen($string_to_write);
                        $bytes_written = fwrite($fh, $string_to_write);

                        if ($bytes_written !== $bytes_to_write) {
                                error_log("ERROR: Unable to write to previous storage file: {$this->previous_run_storage_file}\n");
                                exit(5);
                        }
                }
                if (!fclose($fh)) {
                        error_log("ERROR: Unable to close previous storage file: {$this->previous_run_storage_file}\n");
                        exit(6);
                }
        }

        /**
         * Prints the growing directories list to standard out.
         *
         * Before printing out the list of growing dirs, comb through and
         * reduce the output. If you have a directory whos parent grew, then only list
         * the most specific directory possible. Example where folder 'c' grew by 4 bytes:
         * 4 /a/b/c
         * 4 /a/b
         * 4 /a
         * 4 /
         *
         * Only the first line should be printed. All of those lines are in $growers since
         * they did technically grow, but it's not necessary to see them.
         *
         * @param array growing_directories This is pass by reference just to not make a copy of it,
         *                                  it is not modified in this function.
         */
        private function print_growing_directories(array &$growing_directories) {
                foreach ($growing_directories as $filename => $size) {
                        $parents = $this->find_ordered_parents($growing_directories, $filename);
                        foreach ($parents as $parent_filename => $parent_size) {
                                if ($parent_size === $size) {
                                        unset($growing_directories[$parent_filename]);
                                }
                        }
                }

                foreach ($growing_directories as $name => $bytes) {
                        echo $bytes, "\t", $name, PHP_EOL;
                }
        }

        /**
         * Finds all the parents of a file in the growers list.
         *
         * Example:
         * You give '/a/b/c' as the filename
         *
         * This will give you an associative array with keys:
         * '/' => size in bytes for /
         * '/a' => size in bytes for /a
         * '/a/b' => size in bytes for /a/b
         *
         * @param array growers The growers array. This is pass by reference just to not make a copy of it,
         *                      it is not modified in this function.
         * @param string filename The filename to find parents for
         * @return array A grower-style array of the parents of the filename
         */
        private function find_ordered_parents(array &$growers, $filename) {
                $parents = array();
                while (strlen($filename) > 0) {
                        $filename = substr($filename, 0, strrpos($filename, '/'));
                        if (isset($growers[$filename])) {
                                $parents[$filename] = $growers[$filename];
                        }
                        else {
                                break;
                        }
                }
                ksort($parents);
                return array_reverse($parents);
        }

        /**
         * Prints hlep and usage and exits.
         * If you do not provide an error message, this will exit with status 0.
         * If you provide an error message, this will exit with a non-zero exit status.
         *
         * @param String error_message An optional eror message to print
         */
        private function help($error_message = '') {
                if ($error_message) {
                        echo $error_message, PHP_EOL, PHP_EOL, PHP_EOL;
                }
                $default_storage_file = self::Default_Previous_Run_Storage_File;
                $version_number = self::Version_Number;
                echo <<<USAGE
growing v$version_number by Jordan Sterling

Usage: php growing.php [options]

Required Arguments:
-d, --directory <dir>       The directory to monitor. This will monitor all
                            subdirectories too.:

Optional Arguments:
-t, --tmp-storage <file>    Alternative location to store the previous run's
                            directory sizes. Each run writes a file with the
                            current state so it can be compared to next time.
                                                        This will default to $default_storage_file

-i, --ignore <file>         A path to ignore. This can be repeated. This will
                            ignore the directories listed but can still affect
                            parent sizes like this example directory:

                            dir/ignored_dir

                            If 'ignored_dir' grows in size, this will not report
                            any output. But 'dir' will be reported as growing
                            since it is not in the ignore list.

-?, --help                  Show this message

USAGE;
                // exit 0 if empty message
                $exit_code = 0;
                if (strlen($error_message) > 0) {
                        $exit_code = 1;
                }
                exit($exit_code);
        }
}


$growing = new growing();
$growing->parse_command_line_arguments($argv);
$growing->run();
